
 AccuWeather flow diagram: https://developer.accuweather.com/api-flow-diagram
 
 How to call API in AccuWeather: https://openweathermap.org/current 
 
 Read about JSON objects in Oracle docs:  https://docs.oracle.com/javaee/7/api/index.html 
 
 
   *	javax.json
   *	Interface JsonObject

  Distributing application in JARs:  https://introcs.cs.princeton.edu/java/85application/jar/jar.html 

  the JAR Manifest File: https://www.baeldung.com/java-jar-manifest 
  
  Martin Fowler about Inversion of Control: https://martinfowler.com/articles/injection.html 
  
  Depency Inversion in wikipedia: https://en.wikipedia.org/wiki/Dependency_inversion_principle 
  
  